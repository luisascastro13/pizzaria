/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pizzaria.model;

import java.util.ArrayList;

/**
 *
 * @author luisa
 */
public class Pizza {
    private double pesoBase, valorBase;
    private String nome;
    private char tamanho;
    private ArrayList<Ingrediente> listaIngrediente = new ArrayList<>();
    private Borda borda;    

    public double getPesoBase() {
        return pesoBase;
    }

    public void setPesoBase(double pesoBase) {
        this.pesoBase = pesoBase;
    }

    public double getValorBase() {
        return valorBase;
    }

    public void setValorBase(double valorBase) {
        this.valorBase = valorBase;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public char getTamanho() {
        return tamanho;
    }

    public void setTamanho(char tamanho) {
        this.tamanho = tamanho;
    }

    public void addIngrediente(Ingrediente ingrediente)
    {
        listaIngrediente.add(ingrediente);        
    }
    public void setBorda(Borda borda)
    {
        this.borda = borda;
    }   
    public double pesoTotal()
    {
        //base + ingredientes
        double pesoTotal=0;
        for(int i =0; i<listaIngrediente.size(); i++)
        {
            pesoTotal += listaIngrediente.get(i).getQuantidade();
        }
        pesoTotal += pesoBase;
        pesoTotal = pesoTotal/1000;
        return pesoTotal;
    }
    public double calculaPreco()
    {
        //preco da pizza + preco da borda
        return valorBase + borda.getValor();
    }
    public int getTotalDeIngredientes()
    {
        int totalDeIngredientes = listaIngrediente.size();
        return totalDeIngredientes;
    }
    public void removeIngrediente(Ingrediente ingrediente)
    {
        listaIngrediente.remove(ingrediente);
    }
    
    
                            
}
